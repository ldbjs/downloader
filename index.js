import fs from 'node:fs'
import path from 'node:path'
import followRedirects from 'follow-redirects';
const { http, https } = followRedirects;

import QueueManager from 'ldbjs.queuemanager';
const downloadQueueManager = new QueueManager('DL', 2000, 10);

const DL_TIMEOUT          = parseInt(process.env.MEDIASERVER_DL_TIMEOUT          || 10)   * 1000;
const DL_TIMEOUT_TTL      = parseInt(process.env.MEDIASERVER_DL_TIMEOUT_TTL      || 60)   * 1000;
const DL_TIMEOUT_LIMIT    = parseInt(process.env.MEDIASERVER_DL_TIMEOUT_LIMIT    || 30);
const TTL_BROKEN_DOWNLOAD = parseInt(process.env.MEDIASERVER_TTL_BROKEN_DOWNLOAD || 900)  * 1000;

console.log({
	DL_TIMEOUT,
	DL_TIMEOUT_TTL,
	DL_TIMEOUT_LIMIT,
	TTL_BROKEN_DOWNLOAD,
});

const Statics = {
	brokenDownload: {},
	serverTimeout: {},
}

class Downloader {
	constructor() {
	}
	download(url,dest,logger=null) {
		logger && logger.log("download file", url, dest);
		return downloadQueueManager.execute(url, async () => await this.__download(url,dest,logger))
	}

	__prepareDir(dest) {
		let dir = path.dirname(dest);
		if(!fs.existsSync(dir))
			fs.mkdirSync(dir, { recursive:true });
	}
	__urlToDomain(url) {
		let domain = url.replace(/^(https?:)?\/\/([^\/]+)(\/.*)$/,'$2');
		return domain;
	}
	async __download(url,dest,logger) {
		// Suivi des ressources manquantes
		if(Statics.brokenDownload?.[url])
		{
			if(Statics.brokenDownload[url]?.time > Date.now() - TTL_BROKEN_DOWNLOAD)
			{
				logger && logger.warning("brokenDownload skipped", url, Statics.brokenDownload[url].statusCode);
				return Statics.brokenDownload[url]
			}
			else
			{
				logger && logger.info("brokenDownload refresh", url);
				delete Statics.brokenDownload[url];
			}
		}

		let domain = this.__urlToDomain(url);
		let r = Statics.serverTimeout[domain] || { time:Date.now(), count:0 }
		if(r.count > DL_TIMEOUT_LIMIT && r.time > Date.now() - DL_TIMEOUT_TTL)
		{
			logger && logger.warning("serverTimeout skipped", domain);
			return { time:Date.now(), statusCode:false, file:null };
		}

		if(fs.existsSync(dest))
		{
			let now = Date.now();
			fs.utimesSync(dest,now,now) // immediate lock
		}
		else
		{
			this.__prepareDir(dest);
		}

		let destTmp = dest+'.tmp';
		return new Promise((resolve,reject) => {
			this.__prepareDir(destTmp);

			var file = fs.createWriteStream(destTmp);

			let fetch = http
			if(url.match(/https:\/\//))
				fetch = https

			var request = fetch.get(
				url,
				{
					rejectUnauthorized: false,
					requestCert: false,
					agent: false
				},
				(response) => {
					let statusCode = response.statusCode;
					if(statusCode===200)
					{
						response.pipe(file);
						file.on('finish', () => {
							file.close(() => {

								if(fs.existsSync(dest))
									fs.unlinkSync(dest);
								fs.renameSync(destTmp, dest);

								resolve({
									time:Date.now(),
									statusCode:200,
									file:dest
								})
							});
						});
					}
					else
					{
						logger && logger.warning("brokenDownload", url, statusCode);
						file.close()
						fs.unlinkSync(destTmp);
						let r = { time:Date.now(), statusCode:statusCode, file:null };
						Statics.brokenDownload[url] = r;
						resolve(r)
					}
				}
			);
			// TIMEOUT PART
			request.setTimeout(DL_TIMEOUT, () => {
				let domain = this.__urlToDomain(url);
				logger && logger.info(`Server connection timeout (after ${DL_TIMEOUT/1000} second)`, domain);

				let r = Statics.serverTimeout[domain] || { time:Date.now(), count:0 }
				if(r.time > Date.now() - DL_TIMEOUT_TTL)
				{
					r.time = Date.now()
					r.count++
				}
				Statics.serverTimeout[domain] = r;
				request.abort();
			});
			// ERROR
			request.on('error', (e) => {
				logger && logger.warning("brokenDownload", url, e);
				file.close()
				fs.unlinkSync(destTmp);
				let r = { time:Date.now(), statusCode:false, file:null }
				Statics.brokenDownload[url] = r;
				resolve(r)
			});
		})
	}
}

export default Downloader;